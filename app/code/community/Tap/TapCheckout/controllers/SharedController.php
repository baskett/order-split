<?php 

class Tap_TapCheckout_SharedController extends Tap_TapCheckout_Controller_Abstract
{
   
    protected $_redirectBlockType = 'tapcheckout/shared_redirect';
    protected $_paymentInst = NULL;
	
	
	public function  successAction()
    {
        $response = $this->getRequest()->getPost();
        $queryParam = 'result='.$response['result'];
        $queryParam .= '&payid='.$response['payid'];
        $queryParam .= '&ref='.$response['ref'];
        $queryParam .= '&trackid='.$response['trackid'];
        $queryParam .= '&crdtype=';
        $queryParam .= '&amt=';
        $queryParam .= '&crd=';
        $queryParam .= '&hash=';
        //$redirectParam['_secure'] = true;
		Mage::getModel('tapcheckout/shared')->getResponseOperation($response);
        $this->_redirect('checkout/onepage/success',array('_query'=>$queryParam));
    }
	
	
	
	 public function failureAction()
    {
       
        $arrParams = $this->getRequest()->getPost();
        $queryParam = 'result=FAILED';
        $queryParam .= '&payid='.$response['payid'];
        $queryParam .= '&ref='.$response['ref'];
        $queryParam .= '&trackid='.$response['trackid'];
        $queryParam .= '&crdtype=';
        $queryParam .= '&amt=';
        $queryParam .= '&crd=';
        $queryParam .= '&hash=';
        //$redirectParam['_secure'] = true;
        Mage::getModel('tapcheckout/shared')->getResponseOperation($arrParams);
        $this->getCheckout()->clear();
        $this->_redirect('checkout/onepage/failure', array('_query'=>$queryParam));
    }


    public function canceledAction()
    {
	    $arrParams = $this->getRequest()->getParams();
	
       
        Mage::getModel('tapcheckout/shared')->getResponseOperation($arrParams);
		
		$this->getCheckout()->clear();
		$this->loadLayout();
        $this->renderLayout();
    }


   

    
}
    
    