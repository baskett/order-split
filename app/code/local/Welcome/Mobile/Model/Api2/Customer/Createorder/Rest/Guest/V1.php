<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for customer wishlist
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
require_once 'Welcome/Knet/Block/e24PaymentPipe.inc.php';
class Welcome_Mobile_Model_Api2_Customer_Createorder_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Customer_Createorder
{
   protected function _create()
    {
		//error_reporting(-1);
		//ini_set('display_errors', 'On');
        $post = $this->getRequest()->getBodyParams();
        if ($post) {
            if($post['payment']=='burgan_cc'){
                $post['payment']='cybersourcesa';
            }
        	if($post['lang']){
                $store = Mage::getModel('core/store')->load($post['lang']);
                $storeId = $store->getStoreId();
                $websiteId = $store->getWebsiteId();
                $lang = (strchr($post['lang'], 'ar') == false) ? 1 : 3 ;
            }
            else{
            	$storeId = 19;
                $websiteId = 1;
                $lang = 1;  
            }

        	$customer_id = $post['customer_id'];
        	Mage::app()->setCurrentStore($storeId);
        	$base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            $base_dir = Mage::getBaseDir();
            $decrypt = Mage::getSingleton('core/encryption');
            $customer_id = pack("H*", $customer_id);
         	$customer_id = $decrypt->decrypt($customer_id);
		    $customer = Mage::getSingleton('customer/customer')->setWebsiteId($websiteId)->load($customer_id);
		    if($customer->getId()){
                $quote_id = $post['quote_id'];
                Mage::register('quote_id',$quote_id);
                Mage::register('store_id',$storeId);
                $shipping_address_id = $post['shipping_address_id'];
                $shipping_method = $post['shipping_method'];
                //$shipping_amount = $post['shipping_amount'];
                $payment = $post['payment'];
                
		    	try{
		    		$store = Mage::getModel('core/store')->load($storeId);
                    $quote = Mage::getModel('sales/quote')->setStore($store)->load($quote_id);
                      
                    if($quote->getId() && $quote->getIsActive()){//&& $quote->getIsActive()
                    	$address = Mage::getModel('customer/address')->load($shipping_address_id);
                    	$billingAddress = $quote->getBillingAddress()->addData($address->getData());
                    	$shippingAddress = $quote->getShippingAddress()->addData($address->getData());
                    	$shippingAddress->setCollectShippingRates(true)
						                ->collectShippingRates()
						                ->setShippingMethod($shipping_method)
						                ->setPaymentMethod($payment);
						$quote->getPayment()->importData(array('method' => $payment));
						$quote->collectTotals()->save();
						$service = Mage::getModel('sales/service_quote', $quote);
						$service->submitAll();
						$increment_id = $service->getOrder()->getRealOrderId();
						$order_id = $service->getOrder()->getId();
						$quote->setIsActive(0)->save();
						$quote = $customer = $service = null;
						$order = Mage::getModel("sales/order")->load($order_id);
						$order->setPaymentMethod($payment);
						if($post['payment'] !='knet_cc' && $post['payment'] !='burgan_cc'){
							$order->save();
							$order->sendNewOrderEmail();
						}
						$price  = $order->getGrandTotal();
						//for knet payment only
						$pay = array();
						if($post['payment']=='knet_cc'){
							$security_key = Mage::getModel('Welcome_Knet_Model_Cc')->getConfigData('security_key');
				        	//$base_dir = Mage::getBaseDir();
				        	$trackid_rand = rand(11111111,99999999);
				        	$price  = $order->getGrandTotal();


				        	if($websiteId != 1){
		                        $cur_code = Mage::app()->getStore()->getCurrentCurrencyCode();
		                        $to_code = 'KWD';
		                        $price = Mage::helper('directory')->currencyConvert($order->getBaseGrandTotal(), $cur_code, $to_code);
		                    }
		                    $price = number_format($price, 2, '.', '');
				        	$Pipe = new e24PaymentPipe;
					        $Pipe->setAction(1);
					        $Pipe->setCurrency(414);
					        $Pipe->setLanguage('ENG'); //change it to "ARA" for arabic language
						    $Pipe->setResponseURL($base_url."msuccess.php");
					        $Pipe->setErrorURL($base_url."merror.php");
					        $Pipe->setAmt($price);
					        $Pipe->setResourcePath($base_dir."/knetresource/");
					        $Pipe->setAlias($security_key);
					        $Pipe->setTrackId($trackid_rand);
					        $Pipe->setUdf1($increment_id);
					        $Pipe->setUdf2($storeId);
					        $Pipe->setUdf3("");
					        $Pipe->setUdf4("");
					        $Pipe->setUdf5("");
					        
					        //get results
					        if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>'', 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('knet/knet')->setData($data);
					            $model->save();
					            //$this->_debug($Pipe->getErrorMsg());
					            $errpaymsg = Mage::helper('checkout')->__("There is an issue with your payment process. Kindly contact the site administrator.");
					            $pay = array('payment_status'=>'error','payment_message'=>$errpaymsg);
					        } else {
					            $payID = $Pipe->getPaymentId();
					            $payURL = $Pipe->getPaymentPage();
					            //$payURL = 'www.google.com';

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>$payID, 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('knet/knet')->setData($data);
					            $model->save();
					            $order->setPaymentid($payID);
            					$order->save();
					            $location = $payURL."?PaymentID=".$payID;
					            $pay = array('payment_status'=>'success','payment_url'=>$location);
					        }
				        }
				        // burgan payment functionality
				        if($post['payment']=='burgan_cc'){
				        	$security_key = Mage::getModel('Welcome_Burgan_Model_Cc')->getConfigData('security_key');
				        	//$base_dir = Mage::getBaseDir();
				        	$trackid_rand = rand(11111111,99999999);
				        	$price  = $order->getGrandTotal();
				        	$Pipe = new e24PaymentPipe;
					        $Pipe->setAction(1);
					        $Pipe->setCurrency(414);
					        $Pipe->setLanguage('ENG'); //change it to "ARA" for arabic language
						    $Pipe->setResponseURL($base_url."mbsuccess.php");
					        $Pipe->setErrorURL($base_url."mberror.php");
						    if($websiteId != 1){
					            $cur_code = Mage::app()->getStore()->getCurrentCurrencyCode();
		                        $to_code = 'KWD';
		                        $price = Mage::helper('directory')->currencyConvert($order->getBaseGrandTotal(), $cur_code, $to_code);
					        }
					        $price = number_format($price, 2, '.', '');
					        $Pipe->setAmt($price);
					        $Pipe->setResourcePath($base_dir."/burganresource/");
					        $Pipe->setAlias($security_key);

					        $Pipe->setTrackId($trackid_rand);
					        $Pipe->setUdf1($increment_id);
					        $Pipe->setUdf2($storeId);
					        $Pipe->setUdf3("");
					        $Pipe->setUdf4("");
					        $Pipe->setUdf5("");
					        
					        //get results
					        if($Pipe->performPaymentInitialization()!=$Pipe->SUCCESS){

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>'', 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('burgan/burgan')->setData($data);
					            $model->save();
					            //$this->_debug($Pipe->getErrorMsg());
					            $errpaymsg = Mage::helper('checkout')->__("There is an issue with your payment process. Kindly contact the site administrator.");
					            $pay = array('payment_status'=>'error','payment_message'=>$errpaymsg);
					        } else {
					            $payID = $Pipe->getPaymentId();
					            $payURL = $Pipe->getPaymentPage();
					            //$payURL = 'www.google.com';

					            $data = array('order_id'=>$increment_id,'amount'=>$price, 'paymentid'=>$payID, 'result'=>'','postdate'=>'','tranid'=>'','auth'=>'','ref'=>'', 'trackid' =>$trackid_rand, 'udf1' =>$increment_id, 'udf2' =>'', 'udf3' =>'', 'udf4' =>'', 'udf5' =>'');
					            $model = Mage::getModel('burgan/burgan')->setData($data);
					            $model->save();
					            $order->setPaymentid($payID);
            					$order->save();
					            $location = $payURL."?PaymentID=".$payID;
					            $pay = array('payment_status'=>'success','payment_url'=>$location);
					        }
				        }
                        // Cybersourcesa payment functionality
				        if($post['payment']=='cybersourcesa'){
				        	$location = Mage::getUrl('cybersourcesa/process/appredirect/',array('order_id'=>$increment_id));
                            $pay = array('payment_status'=>'success','payment_url'=>$location);
				        }
				        Mage::unregister('quote_id');
                    	Mage::unregister('store_id');
						$increment_id = $order->getRealOrderId();
						$subTotal = $order->getSubtotal();
						$grandTotal = $order->getGrandTotal();

						if(!$order->getMspBaseCashondelivery() || empty($order->getMspBaseCashondelivery())) {
    					 $codfees = 0;
						} else {
						  $codfees = $order->getMspBaseCashondelivery();
						}

						
						$shipping_amount = $order->getShippingAmount();
						$discount = $order->getDiscountAmount();
						$tax = $order->getTaxAmount();
						$orderItems = $order->getItemsCollection();
						$billingAddr = $order->getBillingAddress();
						$shippingAddr = $order->getShippingAddress();
						$shipping_method = $order->getShippingDescription();
						$payment = $order->getPayment()->getMethodInstance()->getTitle();
						//get billing and shipping address
	                    if($billingAddr->getCountryId() == 'KW'){
							$billing = array('billing_address'=> array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
	                    			'firstname' => $billingAddr->getFirstname(),
				                    'middlename' => $billingAddr->getMiddlename(),
				                    'lastname' => $billingAddr->getLastname(),
				                    'company' => $billingAddr->getCompany(),
				                    //'addr_block' => $billingAddr->getAddrBlock(),
				                    //'city' => $billingAddr->getCity(),
				                    'country_id' => $billingAddr->getCountryId(),
				                    'governorate' => $billingAddr->getGovernorate(),
				                    'area' => $billingAddr->getArea(),
				                    'block' => $billingAddr->getBlock(),
				                    'street' => $billingAddr->getStreet1(),
				                    'avenue' => $billingAddr->getAvenue(),
				                    'building' => $billingAddr->getBuilding(),
				                    'appartment' => $billingAddr->getAppartment(),
				                    'tsh_number' => $billingAddr->getTshNumber(),
				                    //'addr_villa' => $billingAddr->getAddrVilla(),
				                    //'addr_avenue' => $billingAddr->getAddrAvenue(),
				                    //'addr_street' => $billingAddr->getAddrStreet(),
				                    //'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
				                    //'addr_floornumber' => $billingAddr->getAddrFloornumber(),
				                    //'state' => $region,
				                    //'street_1' => $billingAddr->getStreet1(),
				                    //'street_2' => $billingAddr->getStreet2(),
				                    //'postcode' => $billingAddr->getPostcode(),
				                    'telephone'=> $billingAddr->getTelephone(),
				                    'mobile'=> $billingAddr->getMobile(),
				                    //'addr_landline' => $billingAddr->getAddrLandline(),
				                    //'fax' => $billingAddr->getFax()
							));
							$shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                    			'firstname' => $shippingAddr->getFirstname(),
                                'middlename' => $shippingAddr->getMiddlename(),
			                    'lastname' => $shippingAddr->getLastname(),
			                    'country_id' => $shippingAddr->getCountryId(),
			                    'company' => $shippingAddr->getCompany(),
                                'area' => $shippingAddr->getArea(),
                                'block' => $shippingAddr->getBlock(),
                                'street' => $shippingAddr->getStreet1(),
                                'avenue' => $shippingAddr->getAvenue(),
                                'building' => $shippingAddr->getBuilding(),
                                'appartment' => $shippingAddr->getAppartment(),
                                'tsh_number' => $shippingAddr->getTshNumber(),
			                    //'addr_block' => $shippingAddr->getAddrBlock(),
				                //'city' => $shippingAddr->getCity(),
			                    //'addr_villa' => $shippingAddr->getAddrVilla(),
			                    //'addr_avenue' => $shippingAddr->getAddrAvenue(),
			                    //'addr_street' => $shippingAddr->getAddrStreet(),
			                    //'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
			                    //'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    //'street_1' => $shippingAddr->getStreet1(),
			                    //'street_2' => $shippingAddr->getStreet2(),
			                    //'postcode' => $shippingAddr->getPostcode(),
			                    'telephone'=> $shippingAddr->getTelephone(),
			                    'mobile'=> $shippingAddr->getMobile(),
			                    //'addr_landline' => $shippingAddr->getAddrLandline(),
			                    //'fax' => $shippingAddr->getFax()
							));
                        }
                        else if($billingAddr->getCountryId() == 'QA'){
                            $billing = array('billing_address'=> array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
	                    			'firstname' => $billingAddr->getFirstname(),
				                    'lastname' => $billingAddr->getLastname(),
				                    'company' => $billingAddr->getCompany(),
				                    'id_number' => $billingAddr->getIdNumber(),
				                	'qa_city' => $billingAddr->getQaCity(),
				                    'country_id' => $billingAddr->getCountryId(),
				                    'addr_villa' => $billingAddr->getAddrVilla(),
				                    'addr_avenue' => $billingAddr->getAddrAvenue(),
				                    'addr_street' => $billingAddr->getAddrStreet(),
				                    'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
				                    'addr_floornumber' => $billingAddr->getAddrFloornumber(),
				                    //'state' => $region,
				                    'address_line_1' => $billingAddr->getStreet1(),
				                    'postcode' => $billingAddr->getPostcode(),
				                    'telephone'=> $billingAddr->getTelephone(),
				                    'addr_landline' => $billingAddr->getAddrLandline(),
				                    'fax' => $billingAddr->getFax(),
				                    'first_id_proof' => $billingAddr->getFirstIdProof(),
			                    	'second_id_proof' => $billingAddr->getSecondIdProof()
							));
							$shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                    			'firstname' => $shippingAddr->getFirstname(),
			                    'lastname' => $shippingAddr->getLastname(),
			                    'country_id' => $shippingAddr->getCountryId(),
			                    'company' => $shippingAddr->getCompany(),
			                    'id_number' => $shippingAddr->getIdNumber(),
				                'qa_city' => $shippingAddr->getQaCity(),
			                    'addr_villa' => $shippingAddr->getAddrVilla(),
			                    'addr_avenue' => $shippingAddr->getAddrAvenue(),
			                    'addr_street' => $shippingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $shippingAddr->getStreet1(),
			                    'postcode' => $shippingAddr->getPostcode(),
			                    'telephone'=> $shippingAddr->getTelephone(),
			                    'addr_landline' => $shippingAddr->getAddrLandline(),
			                    'fax' => $shippingAddr->getFax(),
			                    'first_id_proof' => $shippingAddr->getFirstIdProof(),
			                    'second_id_proof' => $shippingAddr->getSecondIdProof()
							));
                        }
                        else {
                            $billing = array('billing_address'=> array('customer_address_id'=>$billingAddr->getCustomerAddressId(),
	                    			'firstname' => $billingAddr->getFirstname(),
				                    'lastname' => $billingAddr->getLastname(),
				                    'company' => $billingAddr->getCompany(),
				                	'city' => $billingAddr->getQaCity(),
				                    'country_id' => $billingAddr->getCountryId(),
				                    'addr_villa' => $billingAddr->getAddrVilla(),
				                    'addr_avenue' => $billingAddr->getAddrAvenue(),
				                    'addr_street' => $billingAddr->getAddrStreet(),
				                    'addr_flatenumber' => $billingAddr->getAddrFlatenumber(),
				                    'addr_floornumber' => $billingAddr->getAddrFloornumber(),
				                    //'state' => $region,
				                    'address_line_1' => $billingAddr->getStreet1(),
				                    'postcode' => $billingAddr->getPostcode(),
				                    'telephone'=> $billingAddr->getTelephone(),
				                    'addr_landline' => $billingAddr->getAddrLandline(),
				                    'fax' => $billingAddr->getFax()
							));
							$shipping = array('shipping_address'=> array('customer_address_id'=>$shippingAddr->getCustomerAddressId(),
                    			'firstname' => $shippingAddr->getFirstname(),
			                    'lastname' => $shippingAddr->getLastname(),
			                    'country_id' => $shippingAddr->getCountryId(),
			                    'company' => $shippingAddr->getCompany(),
				                'city' => $shippingAddr->getQaCity(),
			                    'addr_villa' => $shippingAddr->getAddrVilla(),
			                    'addr_avenue' => $shippingAddr->getAddrAvenue(),
			                    'addr_street' => $shippingAddr->getAddrStreet(),
			                    'addr_flatenumber' => $shippingAddr->getAddrFlatenumber(),
			                    'addr_floornumber' => $shippingAddr->getAddrFloornumber(),
			                    //'state' => $region,
			                    'address_line_1' => $shippingAddr->getStreet1(),
			                    'postcode' => $shippingAddr->getPostcode(),
			                    'telephone'=> $shippingAddr->getTelephone(),
			                    'addr_landline' => $shippingAddr->getAddrLandline(),
			                    'fax' => $shippingAddr->getFax()
							));
                        }

						$itemDet = array();
						foreach ($orderItems as $_item)
                    	{
                        $itemDet[] = array('item_id'=>$_item->getId(),
                                            'entity_id'=>$_item->getProductId(),
                                            'name' => $_item->getName(),
                                            'quantity'=> $_item->getQtyOrdered(),
                                            'price' => $_item->getPrice(),
                                            'subtotal' => $_item->getRowTotal(),
                                            );    
                    	}
						$total = array();

	                    $msgsuccess = ($lang == 3) ? Mage::helper('customer')->__('تم حفظ النظام بنجاح') : Mage::helper('customer')->__('The order has been saved successfully');

	                    // if($websiteId == 2){
	                    // $msgsuccess = ($lang == 3) ? Mage::helper('customer')->__('تم حفظ النظام بنجاح') : Mage::helper('customer')->__('The order has been saved successfully');
	                    // }

                        $orderStatus = $order->getStatus();
                        if(strtolower($orderStatus) == 'processing'){
                            $orderStatus = "Payment Captured";
                        }
	                    $info = array('status' => 'Success', 'message' => $msgsuccess, 
	                    			'order_id'=>$increment_id, 
	                    			'order_date'=>$order->getCreatedAt(),
	                    			'order_status'=>$order->getStatus(),
	                    			'shipping_method'=>$shipping_method,
	                    			'payment_method'=>$payment);
	                    $total = array('subtotal'=>$subTotal,'shipping'=>$shipping_amount);
	                    if($discount!=0){
	                    	$total[] = $discount;
	                    }
	                    if($tax != 0){
	                    	$total[] = $tax;
	                    }
	                    $total['codfees'] = $codfees;
	                    $total['grandtotal'] = $grandTotal;
	                    $json = array_merge($info, $billing, $shipping, array('ordered_items'=>$itemDet), array('total'=>$total));
	                    if(!empty($pay)){
	                    	$json = array_merge($json, $pay);
	                    }
	                    echo json_encode($json);
	                    exit();
	                }
	                else{
	                	$msgerror = ($lang == 3) ? Mage::helper('customer')->__("الاقتباس غير صالحة معرف") : Mage::helper('checkout')->__('Invalid Quote Id');
				    	$json = array('status' => 'error', 'message' => $msgerror);
	                    echo json_encode($json);
	                    exit();	
	                }
			    }catch(Exception $e){
			    	$msgerror = $e->getMessage();
			    	$json = array('status' => 'error', 'message' => $msgerror);
                    echo json_encode($json);
                    exit();
			    }
	        }
	        else{
	        	$msgerror = ($lang == 3) ? Mage::helper('customer')->__("العميل غير صالح") : Mage::helper('customer')->__('Invalid customer');
		    	$json = array('status' => 'error', 'message' => $msgerror);
                echo json_encode($json);
                exit();
	        }
    	}
    }
}