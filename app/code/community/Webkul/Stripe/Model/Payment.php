<?php

require_once Mage::getBaseDir('lib').DS.'stripe-php-2.2.0'.DS.'init.php';

class Webkul_Stripe_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'webkul_stripe';
    protected $_isGateway = true;
    protected $_canCapture = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;

    public function __construct()
    {
        if ($this->getConfigData('debug') == 1) {
            \Stripe\Stripe::setApiKey($this->getConfigData('api_test_key'));
        } else {
            \Stripe\Stripe::setApiKey($this->getConfigData('api_live_key'));
        }
    }

    public function capture(Varien_Object $payment, $amount)
    {
        $order = $payment->getOrder();
        $params = Mage::app()->getRequest()->getParams();

        $token = $params['payment']['webkul_stripe_token'];

        try {
            $charge = \Stripe\Charge::create(
                array(
                    'amount' => $amount * 100, // amount in cents, again
                    'currency' => strtolower($order->getBaseCurrencyCode()),
                    'source' => $token,
                    'description' => sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail()),
                )
            );

            $charge = (array) $charge;
            foreach ($charge as $key => $value) {
                if (strpos($key, 'values') !== false) {
                    $charge = $value;
                    $charge['amount'] = $charge['amount']/100;
                    $charge['source'] = json_encode((array) $charge['source']);
                    $charge['metadata'] = json_encode((array) $charge['metadata']);
                    $charge['refunds'] = json_encode((array) $charge['refunds']);
                    $charge['fraud_details'] = json_encode((array) $charge['fraud_details']);
                }
            }
            if ($charge['captured'] != 1) {
                $ch = \Stripe\Charge::retrieve($charge['id']);
                $ch->capture();
                $chargeData = (array) $ch;
                foreach ($chargeData as $key => $value) {
                    if (strpos($key, 'values') !== false) {
                        $chargeData = $value;
                        $chargeData['amount'] = $charge['amount']/100;
                        $chargeData['source'] = json_encode((array) $chargeData['source']);
                        $chargeData['metadata'] = json_encode((array) $chargeData['metadata']);
                        $chargeData['refunds'] = json_encode((array) $chargeData['refunds']);
                        $chargeData['fraud_details'] = json_encode((array) $chargeData['fraud_details']);
                    }
                }

                if ($chargeData['status'] != 'paid' && $chargeData['failure_message'] != '') {
                    Mage::throwException(
                        Mage::helper('paygate')->__('Payment capturing error.').$chargeData['failure_message']
                    );
                }
            } elseif ($charge['status'] != 'paid' && $charge['failure_message'] != '') {
                Mage::throwException(
                    Mage::helper('paygate')->__('Payment capturing error.').$charge['failure_message']
                );
            }
        } catch (\Stripe\Error $e) {
            $this->debugData($e->getMessage());
            Mage::throwException(Mage::helper('paygate')->__('Payment capturing error.').$e->getMessage());
        }catch(Exception $e){

        }
        $payment
            ->setTransactionId($charge['id'])
            ->setIsTransactionClosed(1)
            ->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $charge);

        return $this;
    }

    public function refund(Varien_Object $payment, $amount)
    {
        $transactionId = $payment->getParentTransactionId();
        $order = $payment->getOrder();
        $billing = $order->getBillingAddress()->getData();
        try {
            $charge_data = \Stripe\Charge::retrieve($transactionId);
            $charge = $charge_data->refund(
                array(
                    'amount' => $amount * 100,
                )
            );
            $charge = (array) $charge;
            foreach ($charge as $key => $value) {
                if (strpos($key, 'values') !== false) {
                    $charge = $value;
                    $charge['amount'] = $charge['amount']/100;
                    $charge['amount_refunded'] = $charge['amount_refunded']/100;
                    $charge['metadata'] = json_encode((array) $charge['metadata']);
                }
            }
        } catch (Exception $e) {
            $this->debugData($e->getMessage());
            Mage::throwException(Mage::helper('paygate')->__('Payment refunding error.').$e->getMessage());
        }

        $payment
            ->setTransactionId($charge['id'].'-'.Mage_Sales_Model_Order_Payment_Transaction::TYPE_REFUND)
            ->setParentTransactionId($transactionId)
            ->setIsTransactionClosed(1)
            ->setShouldCloseParentTransaction(1)
            ->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $charge);

        return $this;
    }

    public function isAvailable($quote = null)
    {
        $current_store = Mage::app()->getStore();
        if ($quote && $quote->getBaseGrandTotal() < Mage::getStoreConfig('payment/webkul_stripe/min_order_total', $current_store) && $quote->getBaseGrandTotal() > Mage::getStoreConfig('payment/webkul_stripe/max_order_total', $current_store)) {
            return false;
        }
        $specificcountry = explode(',', Mage::getStoreConfig('payment/webkul_stripe/specificcountry', $current_store));
        if (Mage::getStoreConfig('payment/webkul_stripe/allowspecific', $current_store) != 0) {
            if (!in_array($quote->getBillingAddress()->getCountry(), $specificcountry)) {
                return false;
            }
        }
        if ($this->getConfigData('debug') == 1) {
            return $this->getConfigData('api_test_key', ($quote ? $quote->getStoreId() : null))
            && parent::isAvailable($quote);
        } else {
            return $this->getConfigData('api_live_key', ($quote ? $quote->getStoreId() : null))
            && parent::isAvailable($quote);
        }
    }
}
