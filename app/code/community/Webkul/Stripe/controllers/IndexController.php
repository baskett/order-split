<?php
class Webkul_Stripe_IndexController extends Mage_Core_Controller_Front_Action
{
    public function getGrandTotalAction(){
        $quoteid=$this->getRequest()->getParam('quoteid');
        $quote=Mage::getModel('sales/quote')->load($quoteid);
        $quoteData= $quote->getData();
        $amount=$quoteData['base_grand_total']*100;
        $this->getResponse()->setHeader('Content-type', 'text/html');
        $this->getResponse()->setBody($amount);
    }
}