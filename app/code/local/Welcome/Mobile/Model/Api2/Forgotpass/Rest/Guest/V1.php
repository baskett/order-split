<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * API2 for product categories
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Welcome_Mobile_Model_Api2_Forgotpass_Rest_Guest_V1 extends Welcome_Mobile_Model_Api2_Forgotpass
{   
	protected function _retrieve()
    {
        $response = array();
        $post = $this->getRequest()->getParams();
        //$storeId = ($post['lang'] == 'ar') ? 3 : 1;
        if($post['lang']){
            //$store = Mage::getModel('core/store')->load($post['lang']);
            $store = Mage::app()->getStore($post['lang']);
            $storeId = $store->getStoreId();
            $websiteId = $store->getWebsiteId();
            $lang = (strpos($post['lang'], 'ar') === false) ? 1 : 3 ;
        }
        else{
            $storeId = 19;
            $websiteId = 1;
            $lang = 1;
        }
        Mage::app()->setCurrentStore($storeId);
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                //$postObject = new Varien_Object();
                //$postObject->setData($post);

                $error = false;
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                    $message = ($lang == 3) ? Mage::helper('customer')->__('عنوان البريد الإلكتروني غير صالح') : Mage::Helper('customer')->__('Invalid Email Address');
                }

                if ($error) {
                    throw new Exception();
                }
                $customer = $this->_getModel('customer/customer')
                ->setWebsiteId($websiteId)
                ->loadByEmail($post['email']);

                if ($customer->getId()) {
                    try {
                        $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                        $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                        $customer->sendPasswordResetConfirmationEmail();
                        $reseturl = Mage::getUrl().'customer/account/resetpassword/?id='.$customer->getId().'&token='.$newResetPasswordLinkToken;
                    } catch (Exception $exception) {
                        $error = true;
                        $message = ($lang == 3) ? Mage::helper('customer')->__('غير قادر على معالجة طلبك . الرجاء معاودة المحاولة في وقت لاحق') : Mage::helper('customer')->__('Unable to process your request. Please, try again later');
                    }       
                }else{
					$error = true;
					$translate->setTranslateInline(true);
					$response['mail_status'] = 'error';
					$message = ($lang == 3) ? Mage::helper('customer')->__('معرف البريد الإلكتروني غير صالح .') : Mage::helper('customer')->__('Invalid email id.');
					$response['message'] = Mage::helper('customer')->__($message);
				}
                if ($error) {
                    throw new Exception();
                }
                $translate->setTranslateInline(true);
                $response['mail_status'] = 'success';
                $response['resetpassword_url'] = $reseturl;
                $response['message'] = ($lang == 3) ? Mage::helper('customer')->__('إذا كان هناك حساب المرتبطة %s الصورة سوف تتلقى رسالة بريد إلكتروني مع وصلة ل إعادة تعيين كلمة المرور الخاصة بك .', $post['email']) :  Mage::Helper('customer')->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                        $post['email']); 
                
            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                $response['mail_status'] = 'error';
                $response['message'] = Mage::helper('customer')->__($message);
            }
        } 
        return $response;
    }
    public function _getModel($path, $arguments = array())
    {
        return Mage::getModel($path, $arguments);
    }
}
