<?php
/**
 * Appmerce - Applications for Ecommerce
 * http://www.appmerce.com
 *
 * @extension   MasterCard Internet Gateway Service (MIGS) - Virtual Payment Client
 * @type        Payment method
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Magento
 * @package     Appmerce_Migs
 * @copyright   Copyright (c) 2011-2014 Appmerce (http://www.appmerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Appmerce_Migs_Model_Source_Cctypes
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => 'Amex',
                'label' => 'American Express Credit Card'
            ),
            array(
                'value' => 'AmexPurchaseCard',
                'label' => 'American Express Corporate Purchase Card'
            ),
            array(
                'value' => 'Bankcard',
                'label' => 'Bankcard Credit Card'
            ),
            array(
                'value' => 'Dinersclub',
                'label' => 'Diners Club Credit Card'
            ),
            array(
                'value' => 'GAPcard',
                'label' => 'GAP Inc, Card'
            ),
            array(
                'value' => 'JCB',
                'label' => 'JCB Credit Card'
            ),
            array(
                'value' => 'Loyalty',
                'label' => 'Loyalty Card'
            ),
            array(
                'value' => 'Mastercard',
                'label' => 'MasterCard Credit Card'
            ),
            array(
                'value' => 'MastercardDebit',
                'label' => 'MasterCard Debit Card'
            ),
            array(
                'value' => 'Mondex',
                'label' => 'Mondex Card'
            ),
            array(
                'value' => 'PrivateLabelCard',
                'label' => 'Private Label Card'
            ),
            array(
                'value' => 'SafeDebit',
                'label' => 'SafeDebit Card'
            ),
            array(
                'value' => 'Solo',
                'label' => 'SOLO Credit Card'
            ),
            array(
                'value' => 'Style',
                'label' => 'Style Credit Card'
            ),
            array(
                'value' => 'Switch',
                'label' => 'Switch Credit Card'
            ),
            array(
                'value' => 'VisaDebit',
                'label' => 'Visa Debit Card'
            ),
            array(
                'value' => 'Visa',
                'label' => 'Visa Credit Card'
            ),
            array(
                'value' => 'VisaPurchaseCard',
                'label' => 'Visa Corporate Purchase Card'
            ),
        );
    }

}
