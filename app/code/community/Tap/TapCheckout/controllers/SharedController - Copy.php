<?php 

class Tap_TapCheckout_SharedController extends Tap_TapCheckout_Controller_Abstract
{
   
    protected $_redirectBlockType = 'tapcheckout/shared_redirect';
    protected $_paymentInst = NULL;
	
	
	public function  successAction()
    {
        $response = $this->getRequest()->getPost();
        $redirectParam['result'] = $response['result'];
        $redirectParam['payment_id'] = $response['payid'];
        //$redirectParam['_secure'] = true;
		Mage::getModel('tapcheckout/shared')->getResponseOperation($response);
        $this->_redirect('checkout/onepage/success',$redirectParam);
    }
	
	
	
	 public function failureAction()
    {
       
        $arrParams = $this->getRequest()->getPost();
        $redirectParam['result'] = 'FAILED';
        $redirectParam['payment_id'] = $response['payid'];
        //$redirectParam['_secure'] = true;
        Mage::getModel('tapcheckout/shared')->getResponseOperation($arrParams);
        $this->getCheckout()->clear();
        $this->_redirect('checkout/onepage/failure', $redirectParam);
    }


    public function canceledAction()
    {
	    $arrParams = $this->getRequest()->getParams();
	
       
        Mage::getModel('tapcheckout/shared')->getResponseOperation($arrParams);
		
		$this->getCheckout()->clear();
		$this->loadLayout();
        $this->renderLayout();
    }


   

    
}
    
    