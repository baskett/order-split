<?php
class Aster_Ordersplit_Model_Observer
{

    public function splitOrder(Varien_Event_Observer $observer)
    {
        try {
            $_event = $observer->getEvent();
            $_order = $_event->getOrder();

            if (!$_order) {
                return $this;
            }
            $storeId = $_order->getStoreId();
            $_lastOrderId = $_order->getId();
            $id = $_order->getCustomerId(); // get Customer Id
            $customer = Mage::getModel('customer/customer')->load($id);
            //$quote= $_event->getQuote();
            $quoteId = $_order->getQuoteId();
            //$quote = Mage::getModel('sales/quote')->load($quoteId);
            $quote = $_order->getQuote();
            $incrementId = $_order->getIncrementId();
            $currencyCode = $_order->getOrderCurrencyCode();
            $shippingMethod = $_order->getShippingMethod(); //Fetch the shipping method from order

            $paymentMethodCode = $_order->getPayment()->getMethodInstance()->getCode(); //Fetch the payment method code from order
            
            $sortedItems = array();
            if (is_null($quote)) {
                goto end;
            }
            $items = $quote->getAllItems();
            $parentIds = array();
            foreach ($items as $item) {
                $_product = Mage::getModel('catalog/product')->load((int)$item->getProductId());
                $brandId = $_product->getBrand();
                $products[(int)$item->getId()] = array(
                    'qty' => (int)$item->getQty(),
                    'brand'=>$brandId,
                    'product_id'=>(int)$item->getProductId()
                );
                if ($item->getParentItemId()) {
                    $parentIds[] = $item->getParentItemId();
                }
                unset($brandId);
                unset($_product);
            }
            foreach($parentIds as $ids){
                unset($products[$ids]);
            }
            $productsByBrand = array();
            foreach($products as $productId => $product){
                $productsByBrand[$product['brand']][$product['product_id']] = $product;
            }
            //unset($productsByBrand['brand']);
            foreach($productsByBrand as $brand => $products){
                $transaction = Mage::getModel('core/resource_transaction');
                $reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);
                $order = Mage::getModel('sales/order')
                    ->setIncrementId($reservedOrderId)
                    ->setStoreId($storeId)
                    ->setQuoteId(0)
                    ->setGlobal_currency_code($currencyCode)
                    ->setBase_currency_code($currencyCode)
                    ->setStore_currency_code($currencyCode)
                    ->setOrder_currency_code($currencyCode);
                //Set your store currency USD or any other

                // set Customer data
                $order->setCustomer_email($customer->getEmail())
                    ->setCustomerFirstname($customer->getFirstname())
                    ->setCustomerLastname($customer->getLastname())
                    ->setCustomerGroupId($customer->getGroupId())
                    ->setCustomer_is_guest(0)
                    ->setCustomer($customer);

                // set Billing Address
                $billing = $customer->getDefaultBillingAddress();
                $billingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultBilling())
                    ->setCustomer_address_id($billing->getEntityId())
                    ->setPrefix($billing->getPrefix())
                    ->setFirstname($billing->getFirstname())
                    ->setMiddlename($billing->getMiddlename())
                    ->setLastname($billing->getLastname())
                    ->setSuffix($billing->getSuffix())
                    ->setCompany($billing->getCompany())
                    ->setStreet($billing->getStreet())
                    ->setCity($billing->getCity())
                    ->setCountry_id($billing->getCountryId())
                    ->setRegion($billing->getRegion())
                    ->setRegion_id($billing->getRegionId())
                    ->setPostcode($billing->getPostcode())
                    ->setTelephone($billing->getTelephone())
                    ->setFax($billing->getFax());
                $order->setBillingAddress($billingAddress);

                $shipping = $customer->getDefaultShippingAddress();
                $shippingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultShipping())
                    ->setCustomer_address_id($shipping->getEntityId())
                    ->setPrefix($shipping->getPrefix())
                    ->setFirstname($shipping->getFirstname())
                    ->setMiddlename($shipping->getMiddlename())
                    ->setLastname($shipping->getLastname())
                    ->setSuffix($shipping->getSuffix())
                    ->setCompany($shipping->getCompany())
                    ->setStreet($shipping->getStreet())
                    ->setCity($shipping->getCity())
                    ->setCountry_id($shipping->getCountryId())
                    ->setRegion($shipping->getRegion())
                    ->setRegion_id($shipping->getRegionId())
                    ->setPostcode($shipping->getPostcode())
                    ->setTelephone($shipping->getTelephone())
                    ->setFax($shipping->getFax());

                $order->setShippingAddress($shippingAddress)
                    ->setShipping_method($shippingMethod);
                /*->setShippingDescription($this->getCarrierName('flatrate'));*/
                /*some error i am getting here need to solve further*/

                //you can set your payment method name here as per your need
                $orderPayment = Mage::getModel('sales/order_payment')
                    ->setStoreId($storeId)
                    ->setCustomerPaymentId(0)
                    ->setMethod($paymentMethodCode);
                    //->setPo_number('PO1234567');
                $order->setPayment($orderPayment);

                // let say, we have 2 products
                //check that your products exists
                //need to add code for configurable products if any
                $subTotal = 0;


                foreach ($products as $productId => $product) {
                    $_product = Mage::getModel('catalog/product')->load($productId);
                    $rowTotal = $_product->getPrice() * $product['qty'];
                    $orderItem = Mage::getModel('sales/order_item')
                        ->setStoreId($storeId)
                        ->setQuoteItemId(0)
                        ->setQuoteParentItemId(NULL)
                        ->setProductId($productId)
                        ->setProductType($_product->getTypeId())
                        ->setQtyBackordered(NULL)
                        ->setTotalQtyOrdered($product['rqty'])
                        ->setQtyOrdered($product['qty'])
                        ->setName($_product->getName())
                        ->setSku($_product->getSku())
                        ->setPrice($_product->getPrice())
                        ->setBasePrice($_product->getPrice())
                        ->setOriginalPrice($_product->getPrice())
                        ->setRowTotal($rowTotal)
                        ->setBaseRowTotal($rowTotal);

                    $subTotal += $rowTotal;
                    $order->addItem($orderItem);
                }

                $order->setParentId($incrementId);
                $order->setSubtotal($subTotal)
                    ->setBaseSubtotal($subTotal)
                    ->setGrandTotal($subTotal)
                    ->setBaseGrandTotal($subTotal);

                $transaction->addObject($order);
                $transaction->addCommitCallback(array($order, 'place'));
                $transaction->addCommitCallback(array($order, 'save'));
                $transaction->save();
                unset($transaction);
            }
            end:
        } catch (Exception $e) {

        }
    }
}
