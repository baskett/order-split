<?php
/**
 * Appmerce - Applications for Ecommerce
 * http://www.appmerce.com
 *
 * @extension   MasterCard Internet Gateway Service (MIGS) - Virtual Payment Client
 * @type        Payment method
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Magento
 * @package     Appmerce_Migs
 * @copyright   Copyright (c) 2011-2014 Appmerce (http://www.appmerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Appmerce_Migs_Model_Api extends Mage_Payment_Model_Method_Abstract
{
    // Magento features
    protected $_isGateway = false;
    protected $_canOrder = false;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = false;
    protected $_canUseInternal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_isInitializeNeeded = true;
    protected $_canFetchTransactionInfo = false;
    protected $_canReviewPayment = false;
    protected $_canCreateBillingAgreement = false;
    protected $_canManageRecurringProfiles = false;

    // Special feature: we do NOT store Cc data in Magento
    protected $_canSaveCc = false;

    // Restrictions
    protected $_allowCurrencyCode = array();

    // Local constants
    const COMMAND_PAY = 'pay';
    const COMMAND_CAPTURE = 'capture';

    const TRANSACTION_SOURCE_INTERNET = 'INTERNET';
    const TRANSACTION_SOURCE_MAILORDER = 'MAILORDER';
    const TRANSACTION_SOURCE_TELORDER = 'TELORDER';

    const SOURCE_SUBTYPE_SINGLE = 'SINGLE';
    const SOURCE_SUBTYPE_INSTALLMENT = 'INSTALLMENT';
    const SOURCE_SUBTYPE_RECURRING = 'RECURRING';

    const MAESTRO_CHQ = 'CHQ';
    const MAESTRO_SAV = 'SAV';

    const VPC_VERSION = '1';

    public function __construct()
    {
        $this->_config = Mage::getSingleton('migs/config');
        return $this;
    }

    /**
     * Return configuration instance
     *
     * @return Appmerce_Migs_Model_Config
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * Validate if payment is possible
     *  - check allowed currency codes
     *
     * @return bool
     */
    public function validate()
    {
        parent::validate();
        $currency_code = $this->getCurrencyCode();
        if (!empty($this->_allowCurrencyCode) && !in_array($currency_code, $this->_allowCurrencyCode)) {
            $errorMessage = Mage::helper('migs')->__('Selected currency (%s) is not compatible with this payment method.', $currency_code);
            Mage::throwException($errorMessage);
        }
        return $this;
    }

    /**
     * Get redirect URL after placing order
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return $this->getApiUrl('placement');
    }

    /**
     * Decide grand total
     *
     * @param $order Mage_Sales_Model_Order
     */
    public function getGrandTotal($order)
    {
        $amount = $order->getBaseGrandTotal();

        $cc = $this->getCurrencyCode();
        switch ($cc) {

            // Three digit currencies
            case 'KWD' :
            case 'BHD' :
            case 'LYD' :
            case 'JOD' :
            case 'TND' :
            case 'IQD' :
            case 'BHD' :
                $amount = $amount * 1000;
                break;

            // Two digit currencies
            default :
                $amount = $amount * 100;
        }

        return round($amount);
    }

    /**
     * Decide currency code type
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return Mage::app()->getStore()->getBaseCurrencyCode();
    }

    /**
     * Get available Cc types
     *
     * @return array
     */
    public function getCcAvailableTypes()
    {
        $availableCcTypes = $this->getCcTypes();
        $selectedCcTypes = $this->getConfigData('cctypes');
        if (empty($selectedCcTypes)) {
            return array();
        }

        $selectedCcTypes = explode(',', $selectedCcTypes);
        foreach ($selectedCcTypes as $key => $value) {
            $ccTypes[$value] = $availableCcTypes[$value];
        }

        return $ccTypes;
    }

    /**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Mage_Payment_Model_Info
     */
    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setCcType($data->getCcType())->setCcOwner($data->getCcOwner())->setCcLast4(substr($data->getCcNumber(), -4))->setCcNumber($data->getCcNumber())->setCcCid($data->getCcCid())->setCcExpMonth($data->getCcExpMonth())->setCcExpYear($data->getCcExpYear())->setCcSsIssue($data->getCcSsIssue())->setCcSsStartMonth($data->getCcSsStartMonth())->setCcSsStartYear($data->getCcSsStartYear());
        return $this;
    }

    /**
     * Prepare info instance for save
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function prepareSave()
    {
        $info = $this->getInfoInstance();
        Mage::getSingleton('core/session')->setCcCidEnc($info->encrypt($info->getCcCid()));
        Mage::getSingleton('core/session')->setCcNumberEnc($info->encrypt($info->getCcNumber()));
        $info->setCcNumber(null)->setCcCid(null);
        return $this;
    }

    /**
     * Post with CURL and return response
     *
     * @param $postUrl The URL with ?key=value
     * @param $postData string Message
     * @return reponse XML Object
     */
    public function curlPost($url, $post = array(), $get = FALSE, $return = FALSE, $auth = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . $get);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $return);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($auth) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $auth['user'] . ":" . $auth['pass']);
        }

        if ($post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post, '', '&'));
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}
